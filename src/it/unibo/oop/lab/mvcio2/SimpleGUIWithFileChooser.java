package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import java.nio.file.*;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.Controller;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    private final JFrame frame = new JFrame("Simple GUI with file chooser");

    public static void main(final String... args) {
        new SimpleGUIWithFileChooser();
     }

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUIWithFileChooser() {

        final JPanel canvas = new JPanel();
        canvas.setLayout(new BorderLayout());
        final JTextArea page = new JTextArea();
        canvas.add(page, BorderLayout.CENTER);
        final JButton save = new JButton("Save");
        final JPanel upper = new JPanel();
        upper.setLayout(new BorderLayout());
        canvas.add(upper, BorderLayout.NORTH);
        canvas.add(save, BorderLayout.SOUTH);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setContentPane(canvas);
        final Controller pad = new Controller();
        final JButton browse = new JButton("Browse...");
        final JTextField currentFile = new JTextField(pad.getCurrentFilePath());
        currentFile.setEditable(false);
        browse.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                JFileChooser filechu = new JFileChooser();
                int res = filechu.showOpenDialog(new JFrame());
                if (res == JFileChooser.APPROVE_OPTION) {
                    pad.setCurrentFile(filechu.getSelectedFile());
                    currentFile.setText(pad.getCurrentFilePath());
                } else if (res != JFileChooser.CANCEL_OPTION) {
                    JOptionPane.showInputDialog(canvas, "Something fuckd up", "Eh niente", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        upper.add(currentFile, BorderLayout.CENTER);
        upper.add(browse, BorderLayout.LINE_END);
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                int n = JOptionPane.showConfirmDialog(save, "buoi savlvare?", "Salvatataggio", JOptionPane.YES_NO_OPTION);
                if (n == JOptionPane.YES_NO_OPTION) {
                    pad.saveChanges(page.getText());
                }
            } 
        });
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }
    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

}
